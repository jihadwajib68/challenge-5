const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const app = express();
const port = 3003;

let users = require('./users.json');
app.use(express.static("public"));
app.use(morgan('dev'));

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/signup', (req, res) => {
    res.render('signup');
});

app.get('/signin', (req, res) => {
    res.render('signin');
});

app.get('/game', (req, res) => {
    res.render('game');
});

app.get('/update', (req, res) => {
    res.render('update');
});

app.get('/api/v1/users', (req, res) => {
    res.status(200).json(users);
});

app.get('/api/v1/users/:id', (req, res) => {
    const user =users.find(i => i.id === +req.params.id);

    if(user) {
        res.status(200).json(user);
    } else {
        res.status(404).json({
            'status': 404,
            'message': 'User not found'
        });
    }
});

app.get('/api/v1/*', (req, res) => {
    res.status(404).json({
        'status': 404,
        'message': 'Data not found'
    });
});

app.listen(port, () => {
    console.log('Sedang menggunakan server di port 5000')
})